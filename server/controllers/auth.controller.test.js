const isRemove = require('./editor.controller');

test('EditorComponent renders the text inside it', () => {
  const editor = { id: 1, done: false, name: 'Graham Smith' };
  const wrapper = mount(
    <Editor editor={editor} />
  );
  const p = wrapper.find('.toggle-editor');
  expect(p.text()).toBe('Graham Smith');
});