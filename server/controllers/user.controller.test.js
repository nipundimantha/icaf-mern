const user = require('./user.controller');

test('UserComponent calls doneChange when user is clicked', () => {
    const todo = { id: 1, done: false, name: 'Graham Smith' };
    const doneChange = jest.fn();
    const wrapper = mount(
      <User user={user} doneChange={doneChange} />
    );
  
    const p = wrapper.find('.toggle-todo');
    p.simulate('click');
    expect(doneChange).toBeCalledWith(1);
  });