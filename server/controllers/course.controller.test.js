const signin = require('./admin.controller');

describe('call signin function', () => {
  it('calls the passed function', () => {
    callMyFunction(callback);
    expect(callback).toHaveBeenCalledTimes(1);
  });
});

describe('call hasAuthorization function', () => {
  it('calls the passed function', () => {
    callMyFunction(callback);
    expect(callback).toHaveBeenCalledTimes(1);
  });
});