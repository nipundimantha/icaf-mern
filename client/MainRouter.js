import React from 'react'
import {Route, Switch} from 'react-router-dom'
import Home from './core/Home'
import Users from './user/Users'
import Signup from './user/Signup'
import Signin from './auth/Signin'
import EditProfile from './user/EditProfile'
import Profile from './user/Profile'
import PrivateRoute from './auth/PrivateRoute'
import Menu from './core/Menu'
import NewCourse from './conference/NewCourse'
import AdminDashboard from './admin/adminDashboard'
//import Courses from './conference/Courses'
import Course from './conference/Course'
import EditCourse from './conference/EditCourse'
import MyCourses from './conference/MyCourses'
import Enrollment from './enrollment/Enrollment'
import SignupEditor from './editor/editor-signup'
import SignupReviewer from './reviewer/reviewer-signup'
import SignupAdmin from './admin/admin-signup'
import AttendeeDashboard from './attendee/attendeeDashboard'
import AttendeeEnrollment from './attendee/attendeeEnrollement'
import SignupPresenter from './editor/editor-signup'
import ReviewerDashboard from './researcher/researcher-Dashboard'
import Research from './researcher/research'
import Editor from './conference/editor'


const MainRouter = () => {
    return (<div>
      <Menu/>
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route path="/users" component={Users}/>
        <Route path="/signup" component={Signup}/>
        <Route path="/signupPresenter" component={SignupPresenter}/>
        <Route path="/signupReviewer" component={SignupReviewer}/>
        <Route path="/signupAdmin" component={SignupAdmin}/>
        <Route path="/signin" component={Signin}/>
        <Route path="/adminDashboard" component={AdminDashboard}/>
        <Route path="/attendeeDashboard" component={AttendeeDashboard}/>
        <PrivateRoute path="/user/edit/:userId" component={EditProfile}/>
        <Route path="/user/:userId" component={Profile}/>
        <Route path="/conference/:courseId" component={Course}/>
        <PrivateRoute path="/teach/courses" component={MyCourses}/>
        <Route path="/admin/selection" component={AdminDashboard}/>
        <Route path="/reviewer/selection" component={ReviewerDashboard}/>

        <PrivateRoute path="/teach/conference/new" component={NewCourse}/>
        <PrivateRoute path="/teach/conference/edit/:courseId" component={EditCourse}/>
        <PrivateRoute path="/teach/conference/:courseId" component={Editor}/>
        <PrivateRoute path="/reviewer/research/:courseId" component={Research}/>
        <PrivateRoute path="/learn/:enrollmentId" component={Enrollment}/>
        <PrivateRoute path="/attendee/:enrollmentId" component={AttendeeEnrollment}/>

      </Switch>
    </div>)
}

export default MainRouter
