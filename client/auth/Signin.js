import React, {useState} from 'react'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import Icon from '@material-ui/core/Icon'
import { makeStyles } from '@material-ui/core/styles'
import auth from './../auth/auth-helper'
import {Redirect} from 'react-router-dom'
import {signin} from './api-auth.js'
import {Route, Switch} from 'react-router-dom'

const useStyles = makeStyles(theme => ({
  card: {
    maxWidth: 600,
    margin: 'auto',
    textAlign: 'center',
    marginTop: theme.spacing(12),
    paddingBottom: theme.spacing(2)
  },
  error: {
    verticalAlign: 'middle'
  },
  title: {
    marginTop: theme.spacing(2),
    color: theme.palette.openTitle
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 500,
    borderRadius: 550
  },
  submit: {
    margin: 'auto',
    marginBottom: theme.spacing(2)
  }
}))

export default function Signin(props) {
  const classes = useStyles()
  const [values, setValues] = useState({
      email: '',
      password: '',
      error: '',
      redirectToReferrer: false,
      redirectToReferrerEditor: false,
      redirectToReferrerReviewer: false,
      redirectToReferrerAdmin: false,
      redirectToReferrerResearcher: false,
      redirectToReferrerPresenter: false
  })

  const clickSubmit = () => {
    const user = {
      email: values.email || undefined,
      password: values.password || undefined,
      editor: values.editor || false
    }

    signin(user).then((data) => {
      if (data.error) {
        setValues({ ...values, error: data.error})
      } else {
        auth.authenticate(data, () => {
          console.log(data.user.editor);
          console.log(data.user.admin);
          if(data.user.editor === true){
            setValues({ ...values, error: '',redirectToReferrerEditor: true})
          } else if (data.user.reviewer === true) {
            setValues({ ...values, error: '',redirectToReferrerReviewer: true})
          } else if (data.user.admin === true) {
            setValues({ ...values, error: '',redirectToReferrerAdmin: true})
          } else if (data.user.user === "researcher") {
            setValues({ ...values, error: '',redirectToReferrerResearcher: true})
          } else if (data.user.user === "presenter") {
            setValues({ ...values, error: '',redirectToReferrerPresenter: true})
          } else if (data.user.user === "attendee") {
            setValues({ ...values, error: '',redirectToReferrer: true})
          } else {
            setValues({ ...values, error: '',redirectToReferrer: true})
          }
        })
      }
    })
  }

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value })
  }

  // researcher redirection

  const {fromResearcher} = props.location.state || {
    fromResearcher: {
      pathname: '/attendeeDashboard'
    }
}

const {redirectToReferrerResearcher} = values
if (redirectToReferrerResearcher) {
    return (<Redirect to={fromResearcher}/>)
}

 // Presenter redirection

 const {fromPresenter} = props.location.state || {
  fromPresenter: {
    pathname: '/attendeeDashboard'
  }
}

const {redirectToReferrerPresenter} = values
if (redirectToReferrerPresenter) {
  return (<Redirect to={fromPresenter}/>)
}

//  attendee redirection

  const {from} = props.location.state || {
      from: {
        pathname: '/attendeeDashboard'
      }
  }

  const {redirectToReferrer} = values
  if (redirectToReferrer) {
      return (<Redirect to={from}/>)
  }

  // Editor Redirection

const {fromEditor} = props.location.state || {
  fromEditor: {
        pathname: '/teach/courses'
      }
  }
  
  const {redirectToReferrerEditor} = values
  if (redirectToReferrerEditor) {
    return (<Redirect to={fromEditor}/>)
  }

  // Reviewer Redirection

  const {fromReviewer} = props.location.state || {
    fromReviewer: {
          pathname: '/reviewer/selection'
        }
    }
    
    const {redirectToReferrerReviewer} = values
    if (redirectToReferrerReviewer) {
      return (<Redirect to={fromReviewer}/>)
    }

    // Admin Redirection

    const {fromAdmin} = props.location.state || {
      fromAdmin: {
            pathname: '/admin/selection'
          }
      }
      
      const {redirectToReferrerAdmin} = values
      if (redirectToReferrerAdmin) {
        return (<Redirect to={fromAdmin}/>)
      }

  return (
      <Card className={classes.card}>
        <CardContent>
          <Typography variant="h6" className={classes.title}>
            Sign In
          </Typography>
          <TextField id="email" type="email" label="Email" className={classes.textField} value={values.email} onChange={handleChange('email')} margin="normal"variant="outlined"/><br/>
          <TextField id="password" type="password" label="Password" className={classes.textField} value={values.password} onChange={handleChange('password')} margin="normal" variant="outlined"/>
          <br/> {
            values.error && (<Typography component="p" color="error">
              <Icon color="error" className={classes.error}>error</Icon>
              {values.error}
            </Typography>)
          }
        </CardContent>
        <CardActions>
          <Button color="primary" variant="contained" onClick={clickSubmit} className={classes.submit}>Submit</Button>
        </CardActions>
      </Card>
    )
}
