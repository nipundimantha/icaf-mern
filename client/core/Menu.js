import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import HomeIcon from '@material-ui/icons/Home'
import Library from '@material-ui/icons/LocalLibrary'
import Button from '@material-ui/core/Button'
import auth from './../auth/auth-helper'
import {Link, withRouter} from 'react-router-dom'

const isActive = (history, path) => {
  if (history.location.pathname == path)
    return {color: '#f57c00'}
  else
    return {color: '#fffde7'}
}
const isPartActive = (history, path) => {
  if (history.location.pathname.includes(path))
    return {color: '#fffde7', backgroundColor: '#f57c00', marginRight:10}
  else
    return {color: '#616161', backgroundColor: '#fffde7', border:'1px solid #f57c00', marginRight:10}
}
const Menu = withRouter(({history}) => (
  <AppBar position="fixed" style={{zIndex:12343455}}>
    <Toolbar>
      <Typography variant="h6" color="inherit">
        ICAF
      </Typography>
      <div>
       
      </div>
      <div style={{'position':'absolute', 'right': '10px'}}><span style={{'float': 'right'}}>
        {/* //todo-static UI goes here.... */}
      {
        !auth.isAuthenticated() && (<span>
          {/* original */}
          <Link to="/signup">
            <Button style={isActive(history, "/signup")}>Attendee
            </Button>
          </Link>
          {/* Editor */}
          <Link to="/signupPresenter">
            <Button style={isActive(history, "/signupPresenter")}>Presenter
            </Button>
          </Link>
          {/* //todo */}
          <Link to="/signupReviewer">
            <Button style={isActive(history, "/signupReviewer")}>Researcher
            </Button>
          </Link>
          {/* <Link to="/signupAdmin">
            <Button style={isActive(history, "/signupAdmin")}>Administrator
            </Button>
          </Link> */}
          {/* signIn */}
          <Link to="/signin">
            <Button style={isActive(history, "/signin")}>Sign In
            </Button>
          </Link>
        </span>)
      }
      {
        auth.isAuthenticated() && (<span>
          {auth.isAuthenticated().user.editor && (<Link to="/teach/courses"><Button style={isPartActive(history, "/teach/")}><Library/> Editor</Button></Link>)}
          {/* <Link to="/">
          <IconButton aria-label="Home" style={isActive(history, "/")}>
            <HomeIcon/>
          </IconButton>
        </Link> */}
          <Link to={"/user/" + auth.isAuthenticated().user._id}>
            <Button style={isActive(history, "/user/" + auth.isAuthenticated().user._id)}>My Profile</Button>
          </Link>
          <Button color="inherit" onClick={() => {
              auth.clearJWT(() => history.push('/'))
            }}>Sign out</Button>
        </span>)
      }
      </span></div>
    </Toolbar>
  </AppBar>
))

export default Menu
